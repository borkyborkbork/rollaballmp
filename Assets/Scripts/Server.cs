﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Server : MonoBehaviour
{
    public int port = 6321;

    private List<ServerClient> clients;
    private List<ServerClient> disconnectList;

    private TcpListener server;
    private bool serverStarted;

    public void Init()
    {
        DontDestroyOnLoad(gameObject);
        clients = new List<ServerClient>();
        disconnectList = new List<ServerClient>();

        try
        {
            server = new TcpListener(IPAddress.Any,port);
            server.Start();

            StartListening();
            serverStarted = true;
        }
        catch(Exception e)
        {
            Debug.Log("Socket error:" + e.Message);
        }

    }

    private void Update()
    {
        if (!serverStarted)
        {
            return;
        }
            

        foreach(ServerClient c in clients)
        {
            //Is the client still connected
            if (!IsConnected(c.tcp))
            {
                c.tcp.Close();
                disconnectList.Add(c);
                continue;
            } else
            {
                NetworkStream s = c.tcp.GetStream();

                if (s.DataAvailable)
                {
                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();

                    if (data != null)
                    {
                        OnIncomingData(c, data);
                    }
                }
            }
        }

        for (int i = 0; i < disconnectList.Count - 1; i++)
        {
            // TODO: Tell our player sombody has disconnected

            clients.Remove(disconnectList[i]);
            disconnectList.RemoveAt(i);
        }
    }

    private void StartListening()
    {
        server.BeginAcceptTcpClient(AcceptTcpClient, server);
    }

    private void AcceptTcpClient(IAsyncResult ar)
    {
        // Accept person into room

        TcpListener listener = (TcpListener)ar.AsyncState;

        // Create list of users already connected
        // needs to be created before the new user is added to clients because the new user doesn't have a name yet
        string allUsers = "";
        foreach (ServerClient i in clients)
        {
            allUsers += i.clientName + '|';
        }

        ServerClient sc = new ServerClient(listener.EndAcceptTcpClient(ar));
        clients.Add(sc);

        StartListening();

        Debug.Log("1-Sombody has connected!");

        // Create initial message to sc/client who just connected
        // Send Who Are You string (SWHO) | Send data about who is already connected
   
        Broadcast("SWHO|"+allUsers, clients[clients.Count - 1]);


    }
    
    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);

                return true;
            } else {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    //Server send
    private void Broadcast(string data, List<ServerClient> cl)
    {
        foreach(ServerClient sc in cl)
            try
            {
                StreamWriter writer = new StreamWriter(sc.tcp.GetStream());
                writer.WriteLine(data);
                writer.Flush();
            }
            catch (Exception e)
            {
                Debug.Log("Write error : " + e.Message);
            }
    }
    //Server send - proxy - OVERLOAD
    private void Broadcast(string data, ServerClient c)
    {
        // overload of the Broadcast function to create a list with one single client
        // then calls the other Broadcast function sending just that one client
        List<ServerClient> sc = new List<ServerClient> { c };
        Broadcast(data, sc);
    }
    //Server read
    private void OnIncomingData(ServerClient c, string data)
    {
        Debug.Log("SERVER: "+data);
        // Start parsing the incomming messages
        string[] aData = data.Split('|');

        // Switch on first item in list which is the message type
        switch (aData[0])
        {
            case "CWHO":
                c.clientName = aData[1];
                // Receive and set isHost based on message send up from client
                c.isHost = (aData[2] == "0") ? false : true;
                Broadcast("SCON|" + c.clientName, clients);
                break;
        }
    }
}

public class ServerClient
{
    public string clientName;
    public TcpClient tcp;
    public bool isHost;

    public ServerClient(TcpClient tcp)
    {
        this.tcp = tcp;
    }
}

