﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed = 10;
    public bool isPlayer1; //defaults to false

    private Rigidbody rb;

    void Start()
    {
        // get access to client object - 
        Client c = FindObjectOfType<Client>();
        isPlayer1 = c.isHost;

            
            
        rb = GetComponent<Rigidbody>();
        
    }


    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal,0.0f, moveVertical);

        rb.AddForce(movement*speed); 
    }

}
