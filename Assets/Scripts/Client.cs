﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using UnityEngine;

public class Client : MonoBehaviour
{
    public string clientName;
    // Needs to be public because we will switch it from other code
    public bool isHost;

    private bool socketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;

    private List<GameClient> players = new List<GameClient>();

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public bool ConnectToServer(string host, int port)
    {
        if (socketReady)
        {
            return false;
        }
        try
        {
            socket = new TcpClient(host, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);

            socketReady = true;
        }
        catch(Exception e)
        {
            Debug.Log("Socket error " + e.Message);
        }

        return socketReady;

    }

    private void Update()
    {
        // check every frame
        // don't need loop because this is one client
        if (socketReady)
        {
            if (stream.DataAvailable)
            {
                string data = reader.ReadLine();
                if (data != null)
                    OnIncomingData(data);

            }
        }
    }

    //sending messages to the server
    public void Send(string data)
    {
        if (!socketReady)
        {
            return;
        }

        writer.WriteLine(data);
        writer.Flush();

    }

    //Read new messages from the server
    private void OnIncomingData(string data)
    {
        Debug.Log("CLIENT: "+data);
        // Start parsing the incomming messages
        string[] aData = data.Split('|');

        // Switch on first item in list which is the message type
        switch (aData[0])
        {
            case "SWHO":
                // Loop over list items after message type to get list of clients already connecte to host
                for (int i = 1; i < aData.Length -1; i++)
                {
                    // We know SWHO recipients are not HOST because it's only sent when 
                    // initially connecting so HOST can find out who you are
                    UserConnected(aData[i], false);

                }
                // Need to tell the HOST who you are
                Send("CWHO|" + clientName + "|" + ((isHost)?1:0).ToString());
                break;
            case "SCON":
                // Not sure if this is HOST or not
                UserConnected(aData[1], false);
                break;
        }
    }

    private void UserConnected(string name, bool host)
    {
        GameClient c = new GameClient();
        c.name = name;

        // Each message case calls this to add a player
        players.Add(c);

        if (players.Count == 2)
            GameManager.Instance.StartGame();

    }

    private void OnApplicationQuit()
    {
        CloseSocket();
    }

    private void Ondisable()
    {
        CloseSocket();
    }

    private void CloseSocket()
    {
        if (!socketReady)
            return;

        writer.Close();
        reader.Close();
        socket.Close();
        socketReady = false;
        
    }
}

public class GameClient
{
    // client side definition of user
    public string name;
    public bool isHost;

}